#!/usr/bin/python2

import utils

class GenericEntry(object):
	def parse(self, line):
		self.line = line

	def build(self):
		return self.line

	@staticmethod
	def gotit(line):
		return True

	def __str__(self):
		return self.build()

class UnknownEntry(GenericEntry):
	pass

class DhcpHostEntry(GenericEntry):
	def __init__(self, line = None):
		self.hostname = ''
		self._mac = []
		self.ip = ''
		self.lease = 'infinite'
		self.clientid = ''
		self.ignored = False
		self.set = []
		if line:
			self.parse(line)

	def __del__(self):
		pass

	@staticmethod
	def gotit(line):
		return line.startswith('dhcp-host=')

	@property
	def mac(self):
		s = ''
		for mac in self._mac:
			s = utils.append_comma(s, mac)
		return s

	def append_mac(self, mac):
		if mac in self._mac:
			return
		self._mac.append(mac)

	def append_set(self, value):
		value = value.replace('set:', '')
		if value in self.set:
			return
		self.set.append(value)

	def __str__(self):
		return self.build()

	def parse(self, line):
		if not line.startswith('dhcp-host='):
			raise Exception('not a dhcp-host entry')
		line = line.replace('dhcp-host=', '')
		elements = line.split(',')
		for element in elements:
			element = element.strip()
			if utils.is_mac(element):
				self.append_mac(element)
			elif utils.is_ip(element):
				self.ip = element
			elif utils.is_lease(element):
				self.lease = element
			elif utils.is_id(element):
				self.clientid = element.replace('id:', '')
			elif utils.is_set(element):
				self.append_set(element)
			elif 'ignore' == element:
				self.ignored = True
			else:
				self.hostname = element

	def build(self):
		s = 'dhcp-host='
		for mac in self._mac:
			s = utils.append_comma(s, mac)
		s = utils.append_comma(s, 'id:' + self.clientid)
		s = utils.append_comma(s, self.ip)
		if self.ignored:
			s = utils.append_comma(s, 'ignored')
		s = utils.append_comma(s, self.hostname)
		if self.set:
			s = utils.append_comma(s, 'set:' + self.set[0])
		s = utils.append_comma(s, self.lease)
		return s

class BooleanEntry(GenericEntry):
	def __init__(self, line = None):
		self.name = ''
		self.state = False
		if line:
			self.parse(line)

	def parse(self, line):
		self.name = line
		self.state = True

	def build(self):
		if self.state:
			return self.name
		return ''

	@staticmethod
	def gotit(line):
		if '=' in line:
			return False

		if ' ' in line:
			return False

		return True

class KeyValueEntry(GenericEntry):
	def __init__(self, line = None):
		self.key = ''
		self.value = ''
		if line:
			self.parse(line)

	def parse(self, line):
		self.key, self.value = line.split('=')

	def build(self):
		if self.key:
			return self.key + '=' + self.value
		return ''

	@staticmethod
	def gotit(line):
		if not '=' in line:
			return False

		keyval = line.split('=')
		keyval = filter(None, keyval)
		if 2 != len(keyval):
			return False

		return True

class DnsHostnameEntry(GenericEntry):
	def __init__(self, line = None):
		self.hostname = ''
		self.ip = ''
		if line:
			self.parse(line)

	def parse(self, line):
		keyval = KeyValueEntry(line)
		values = filter(None, keyval.value.split('/'))
		if 2 != len(values):
			raise ValueError('Expected syntax of address=/fqdn/ipaddress')

		hostname, address = values
		if not utils.is_ip(address):
			raise ValueError('Expected ip address')
		self.hostname = hostname
		self.ip = address

	def build(self):
		keyval = KeyValueEntry()
		keyval.key = 'address'
		keyval.value = '/%s/%s' % (self.hostname, self.ip)
		return keyval.build()

	@staticmethod
	def gotit(line):
		if not KeyValueEntry.gotit(line):
			return False
		if not line.startswith('address='):
			return False
		return True

class DnsmasqConfig(object):
	def __init__(self, filename):
		self.filename = filename
		self.reload()
	
	def __del__(self):
		pass

	def reload(self):
		data = open(self.filename, 'rb').read()
		try:
			open(self.filename + '.backup', 'wb').write(data)
		except IOError:
			pass
		self.content = data.split('\n')
		self.entries = self.parse()
	
	def save(self):
		lines = map(str, self.entries)
		config = '\n'.join(lines)
		open(self.filename, 'wb').write(config)

	def parse(self):
		entries = []
		for line in self.content:
			if not line or line.startswith('#'):
				continue
			if BooleanEntry.gotit(line):
				entries.append(BooleanEntry(line))
			elif DhcpHostEntry.gotit(line):
				entries.append(DhcpHostEntry(line))
			elif DnsHostnameEntry.gotit(line):
				entries.append(DnsHostnameEntry(line))
			elif KeyValueEntry.gotit(line):
				entries.append(KeyValueEntry(line))
			else:
				entries.append(GenericEntry(line))
		return entries

	def reserved_addresses(self):
		return filter(lambda x: isinstance(x, DhcpHostEntry), self.entries)

	def dns_hostnames(self):
		return filter(lambda x: isinstance(x, DnsHostnameEntry), self.entries)

	def keyvals(self):
		return filter(lambda x: isinstance(x, KeyValueEntry), self.entries)

	def booleans(self):
		return filter(lambda x: isinstance(x, BooleanEntry), self.entries)

	def unknown_values(self):
		return filter(lambda x: isinstance(x, UnknownEntry), self.entries)
